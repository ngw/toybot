# frozen_string_literal: true

RSpec.describe Toybot::Robot do
  subject(:robot) { described_class.new tabletop: tabletop, x: 0, y: 0, orientation: orientation }

  let(:orientation) { :east }
  let(:tabletop) { Toybot::Tabletop.new }

  describe '#orientation=' do
    context 'when valid' do
      it 'it is set' do
        Toybot::Robot::DIRECTIONS.each do |direction|
          robot.orientation = direction
          expect(robot.orientation).to eq(direction)
        end
      end
    end

    context 'when invalid' do
      it 'is not set' do
        robot.orientation = :wrong
        expect(robot.orientation).to eq(orientation)
      end
    end
  end

  describe '#left!' do
    it 'calculates orientation correctly when from north' do
      robot.orientation = :north
      robot.left!
      expect(robot.orientation).to eq(:west)
    end

    it 'calculates orientation correctly when from west' do
      robot.orientation = :west
      robot.left!
      expect(robot.orientation).to eq(:south)
    end

    it 'calculates orientation correctly when from south' do
      robot.orientation = :south
      robot.left!
      expect(robot.orientation).to eq(:east)
    end

    it 'calculates orientation correctly when from east' do
      robot.orientation = :east
      robot.left!
      expect(robot.orientation).to eq(:north)
    end
  end

  describe '#right!' do
    it 'calculates orientation correctly when from north' do
      robot.orientation = :north
      robot.right!
      expect(robot.orientation).to eq(:east)
    end

    it 'calculates orientation correctly when from east' do
      robot.orientation = :east
      robot.right!
      expect(robot.orientation).to eq(:south)
    end

    it 'calculates orientation correctly when from south' do
      robot.orientation = :south
      robot.right!
      expect(robot.orientation).to eq(:west)
    end

    it 'calculates orientation correctly when from west' do
      robot.orientation = :west
      robot.right!
      expect(robot.orientation).to eq(:north)
    end
  end

  describe '#move' do
    context 'when valid' do
      it 'calculates coords correctly when going north' do
        robot.orientation = :north
        2.times { robot.move }
        expect(robot.y).to eq(2)
      end

      it 'calculates coords correctly when going :east' do
        robot.orientation = :east
        2.times { robot.move }
        expect(robot.x).to eq(2)
      end

      it 'calculates coords correctly going :south' do
        robot = Toybot::Robot.new(x: 0, y: 4, orientation: :south, tabletop: tabletop)
        2.times { robot.move }
        expect(robot.y).to eq(2)
      end

      it 'calculates coords correctly going :west' do
        robot = Toybot::Robot.new(x: 4, y: 0, orientation: :west, tabletop: tabletop)
        2.times { robot.move }
        expect(robot.x).to eq(2)
      end
    end

    context 'when invalid' do
      it 'doesn\'t overrun the columns\' edge' do
        6.times { robot.move }
        expect(robot.x).to eq(4)
        robot.orientation = :west
        6.times { robot.move }
        expect(robot.x).to eq(0)
      end

      it 'doesn\'t overrun the rows edge' do
        robot.orientation = :north
        6.times { robot.move }
        expect(robot.y).to eq(4)
        robot.orientation = :south
        6.times { robot.move }
        expect(robot.y).to eq(0)
      end
    end
  end
end
