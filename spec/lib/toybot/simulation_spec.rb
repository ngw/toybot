# frozen_string_literal: true

RSpec.describe Toybot::Simulation do
  subject(:simulation) { described_class.new(file: instructions_file_path) }

  let(:instructions_file_path) { File.join(File.dirname(__FILE__), '../..', 'fixtures/INSTRUCTIONS') }

  it 'simulates a run correctly' do
    output = "0,1,NORTH\n0,0,WEST\n3,3,NORTH\n2,2,SOUTH\n3,2,NORTH\n3,2,NORTH\n"
    expect { simulation.run }.to output(output).to_stdout
  end
end
