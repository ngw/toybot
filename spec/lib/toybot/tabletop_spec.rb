# frozen_string_literal: true

RSpec.describe Toybot::Tabletop do
  subject(:tabletop) { described_class.new }

  context 'when without arguments' do
    it 'has sane defaults' do
      expect(tabletop.rows).to eq(4)
      expect(tabletop.columns).to eq(4)
    end
  end

  context 'when validating a position' do
    it 'is valid' do
      [{ x: 0, y: 0 }, { x: 1, y: 3 }, { x: 4, y: 2 }, { x: 4, y: 4 }].each do |coords|
        expect(tabletop.valid?(coords)).to be true
      end
    end

    it 'is invalid' do
      [{ x: -1, y: 1 }, { x: 5, y: 4 }, { x: 3, y: 6 }, { x: 2, y: -2 }].each do |coords|
        expect(tabletop.valid?(coords)).to be false
      end
    end
  end
end
