# frozen_string_literal: true

RSpec.describe Toybot do
  it 'has a $VERSION' do
    expect(Toybot::VERSION).to eq('0.1')
  end
end
