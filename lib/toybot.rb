# frozen_string_literal: true

require_relative 'toybot/robot'
require_relative 'toybot/simulation'
require_relative 'toybot/tabletop'

module Toybot
  VERSION = '0.1'
end
