# frozen_string_literal: true

module Toybot
  class Simulation
    def initialize(file:)
      @file = file
      @tabletop = Toybot::Tabletop.new
    end

    def run
      File.open(@file, 'r').each_line do |line|
        execute(line)
      end
    end

    private

    def execute(line)
      token, args = tokenize(line)
      case token
      when :place
        begin
          @robot = Toybot::Robot.new(x: args[:x], y: args[:y], orientation: args[:orientation], tabletop: @tabletop)
        rescue TypeError
          # will just use old @robot which should be in a valid state
        end
      when :move
        @robot.move
      when :right
        @robot.right!
      when :left
        @robot.left!
      when :report
        puts "#{@robot.x},#{@robot.y},#{@robot.orientation.to_s.upcase}"
      end
    end

    def tokenize(line)
      instruction = line.strip.split
      case instruction.first
      when 'PLACE'
        [:place, extract(instruction.last)]
      when 'RIGHT'
        [:right, nil]
      when 'LEFT'
        [:left, nil]
      when 'MOVE'
        [:move, nil]
      when 'REPORT'
        [:report, nil]
      end
    end

    def extract(coords)
      x, y, orientation = coords.split(',')
      { x: x, y: y, orientation: orientation }
    end
  end
end
