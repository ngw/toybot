# frozen_string_literal: true

module Toybot
  class Robot
    DIRECTIONS = %i[north west south east].freeze

    attr_reader :x, :y, :orientation

    def initialize(tabletop:, x:, y:, orientation:)
      @tabletop = tabletop

      raise TypeError, 'Robot is invalid' unless @tabletop.valid?(x: x.to_i, y: y.to_i)

      @x = x.to_i
      @y = y.to_i
      @orientation = orientation.downcase.to_sym
    end

    def left!
      @orientation = { north: :west, west: :south, south: :east, east: :north }[@orientation]
    end

    def right!
      @orientation = { north: :east, east: :south, south: :west, west: :north }[@orientation]
    end

    def orientation=(orientation)
      return unless DIRECTIONS.include?(orientation)

      @orientation = orientation
    end

    def move
      x, y = calculate
      if @tabletop.valid?(x: x, y: y)
        @x = x
        @y = y
        true
      else
        false
      end
    end

    private

    def calculate
      case @orientation
      when :north
        [@x, @y + 1]
      when :south
        [@x, @y - 1]
      when :west
        [@x - 1, @y]
      when :east
        [@x + 1, @y]
      end
    end
  end
end
