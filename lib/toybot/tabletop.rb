# frozen_string_literal: true

module Toybot
  class Tabletop
    attr_accessor :rows, :columns

    def initialize(rows: 4, columns: 4)
      @rows = rows
      @columns = columns
    end

    def valid?(x:, y:)
      x >= 0 && x <= @columns && y >= 0 && y <= @rows
    end
  end
end
